#!/bin/bash
ulimit -Sn `ulimit -Hn` # ROCm is a bitch
conda deactivate > /dev/null 2>&1 # Some things with oneAPI happen with conda. Deactivate conda if it is active to avoid spam.
source ./venv/bin/activate
source /opt/intel/oneapi/setvars.sh
ipexrun ./src/main.py "$@" --training-oneapi
deactivate
